"use strict";

const Koa = require("koa"),
  bodyParser = require("koa-bodyparser"),
  logger = require("koa-logger"),
  helmet = require("koa-helmet"),
  compress = require("koa-compress"),
  { amiStart } = require("./ami"),
  { routes, allowedMethods } = require("./src/middleware/routes"),
  errorHandler = require("./src/middleware/error"),
  { initializeDb } = require("./services/mongo"),
  serve = require("koa-static"),
  { websockified }= require("./src/middleware/wsroutes"),
  { queuestates } = require("./services/postgres"),
  log = require("./services/logger")("app"),
  { port } = require("./config");

log.warn(`App started in ${process.env.NODE_ENV} mode`);

const PORT = process.env.PORT || port || 3000;

const app = websockified(new Koa());

queuestates()

app
  .use(helmet())
  .use(compress())
  .use(logger())
  .use(errorHandler)
  .use(bodyParser())
  .use(serve("./public"))
  .use(routes())
  .use(allowedMethods());

app.on("error", (err, ctx) => log.error("Custom error handler", err, ctx));

initializeDb(err => {
  if (err) {
    console.log(err)
    log.error("Db initializetion failed");
    process.exit(1);
  } else {
    log.info(`Application server listening on ${PORT}`);
    amiStart();
    app.listen(PORT);
  }
});

