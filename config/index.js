const configProduction = require("./config.prod");
const configDevelopment = require("./config.dev");
const fs = require("fs");
const path = require("path");
console.log(process.env.NODE_ENV)
if (process.env.NODE_ENV == "development") {
  if (fs.existsSync(path.resolve(__dirname, "config.local.js"))) {
    const config = require("./config.local");
    module.exports = config;
  } else {
    module.exports = configDevelopment;
  }
} else {
  module.exports = configProduction;
}
