# Документация для Call-center (Selma v2.0)
---
## Для запуска необходимо установить пакеты node_modules и pip указанные в *package.json* и *requirements.txt*
*Koa.js server + Vue.js front*
### Для запуска сервера установи все пакеты
```
npm i
```
### После запусти сервер
```
npm start
```

---
### Для запуска dev-режима находясь в директории [dev](https://bitbucket.org/telefonncrew/selma2/src/master/client) установи все пакеты
```
npm i
```
### После чего запусти сам сервер
```
npm run serve
```
### Для запуска сборки введите из директории [dev](https://bitbucket.org/telefonncrew/selma2/src/master/client)
```
npm run build
```
#### Всё приложение делится на представление (views) и составляющие (components)
##### Иерархия компонентов Vue *~/client/src/components*
###### Весь код придерживается написания однофайловых Vue компонентов

1. **AbonentInfo.vue**
2. **CallerNumber.vue**
3. **CallHistory.vue**
4. **CallToNumber.vue**
5. **OperatorInfo.vue**
6. **Queues.vue**
7. **Status.vue**
8. **WaitTime.vue**
9. **YandexMap.vue**

---
##### Иерархия представлений (views)

1. **Navigation.vue**
    *  Header с навигацией по страницам веб-приложения
2. **Operator.vue**
    *  Главная (рабочая) страница оператора
3. **PhoneBook.vue**
    *  Телефонная книга
4. **CallJournal.vue**
    *  Журнал вызовов

##### store (Vuex)
###### entrypoint - store.js
1. **event.js - редюсер**
    *  Хранилище для работы со входящими телефонными событиями
2. **ui.js - редюсер**
    *  Хранилище для работы с UI составляющей приложения (модальные окна, всплывающие события)

---

##### router (Vue-router)

1. **router.js**
    *  Работа с клиентским роутингом
