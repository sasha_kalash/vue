import Vue from "vue";
import Vuex from "vuex";
import ui from "@/store/ui";
import event from "@/store/event";
import user from "@/store/user";

import { createModule } from "vuex-toast";
import "vuex-toast/dist/vuex-toast.css";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    ui,
    event,
    user,
    toast: createModule({
      dismissInterval: 8000
    })
  }
});
