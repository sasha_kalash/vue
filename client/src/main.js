import Vue from "vue";
import App from "./App.vue";
import axios from "axios";
import router from "./router";
import store from "./store";
import { SET_TOKEN } from "./store/user";

Vue.config.productionTip = false;

const token = localStorage.getItem("token") || "";

if (token) {
  store.commit(SET_TOKEN, token);
  axios.defaults.headers.common["token"] = token;
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
