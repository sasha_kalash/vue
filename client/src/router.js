import Vue from "vue";
import Router from "vue-router";
import Header from "@/views/Navigation";
import Operator from "@/views/Operator";
import PhoneBook from "@/views/PhoneBook";
import AuthView from "@/views/AuthView";
import PageNotFound_404 from "@/views/404";

import { ROLE_ADMIN, ROLE_OPERATOR, ROLE_USER } from "@/constants/role";
import store from "./store";

Vue.use(Router);

const router = new Router({
  routes: [
    {
      name: "operator",
      path: "/",
      components: {
        header: Header,
        default: Operator
      },
      meta: {
        auth: true, // Выставь флаг в true, когда будешь писать с аутентификацией
        role: [ROLE_ADMIN, ROLE_OPERATOR, ROLE_USER]
      }
    },
    {
      name: "phonebook",
      path: "/phonebook",
      components: {
        header: Header,
        default: PhoneBook
      },
      meta: {
        auth: false,
        role: [ROLE_ADMIN, ROLE_OPERATOR, ROLE_USER]
      }
    },
    {
      name: "login",
      path: "/login",
      component: AuthView,
      meta: {
        auth: false
      }
    },
    {
      path: "/404",
      component: PageNotFound_404
    }
  ]
});

router.beforeResolve((to, from, next) => {
  // Выполнится после beforeEach при переходе на url
  if (localStorage.token && to.path === "/login") {
    // необходимо для запрета перехода на /login аутентифицированного юзера
    next(false);
  } else {
    next();
  }
});

router.beforeEach((to, from, next) => {
  console.log("beforeEach");
  if (to.matched.some(record => record.meta.auth)) {
    if (store.state.user.token) {
      next();
    } else {
      next(false);
    }
  } else if (!to.matched.length) {
    next("/404");
  } else {
    next();
  }

  function checkRole(data) {
    return to.matched.some(record =>
      record.meta.role.find(el => el === data.role)
    );
  }
});

export default router;
