export const CALL_TO = "CALL_TO";
export const PICK_UP = "PICK_UP";
export const HANG = "HANG";

export default {
  actions: {
    [CALL_TO]({ state, commit, rootState }, number) {
      rootState.event.websocket.send(JSON.stringify({ "status": "CALL", "number": number }));
    },
    [PICK_UP]({ state, commit, rootState }) {
      rootState.event.websocket.send(JSON.stringify({ "status": "PICKUP" }));
    },
    [HANG]({ state, commit, rootState }) {
      console.log('hang')
      rootState.event.websocket.send(JSON.stringify({ "status": "HANG" }));
    }
  }
}