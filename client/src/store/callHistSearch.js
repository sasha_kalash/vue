export const CALL_HISTORY_SEARCH_REQ = "CALL_HISTORY_SEARCH_REQ";
export const SET_FINDING_CALL_HISTORY = "SET_FINDING_CALL_HISTORY";

export default {
  state: {
    calls: []
  },
  actions: {
    [CALL_HISTORY_SEARCH_REQ]({ state, commit, rootState }, period) {
      rootState.event.websocket.send(JSON.stringify({ "status": CALL_HISTORY_SEARCH_REQ, "data": period }));
    } 
  },
  mutations: {
    [SET_FINDING_CALL_HISTORY](state, payload) {
      console.log(payload)
      state.calls = payload;
    }
  }
}