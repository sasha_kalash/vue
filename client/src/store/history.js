export const ADD_CALL_TO_HISTORY = "ADD_CALL_TO_HISTORY";
export const SET_CALL_HISTORY = "SET_CALL_HISTORY";

export default {
  state: {
    calls: []
  },
  mutations: {
    [ADD_CALL_TO_HISTORY](state, payload) {
      state.calls = [payload, ...state.calls];
    },
    [SET_CALL_HISTORY](state, payload) {
      state.calls = payload.reverse();
    }
  }
}