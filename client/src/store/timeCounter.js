export const START_TIMER = "START_TIMER";
export const STOP_TIMER = "STOP_TIMER";

let connectTimeInterval, workingTimeInterval;

export default {
  state: {
    connectTime: {
      hours: "00",
      min: "00",
      sec: "00"
    },
    workingTime: {
      hours: "00",
      min: "00",
      sec: "00"
    }
  },
  mutations: {
    [START_TIMER](state, eventKey) {
      const currentDate = new Date();
      this.timeInterval = setInterval(() => {
        const newDate = new Date() - currentDate;
        let sec = Math.abs(Math.floor(newDate / 1000) % 60),
        min = Math.abs(Math.floor(newDate / 1000 / 60) % 60),
        hours = Math.abs(Math.floor(newDate / 1000 / 60 / 60) % 24);
        if (sec.toString().length == 1) {
          sec = '0' + sec;
        }
        if (min.toString().length == 1) {
          min = '0' + min;
        }
        if (hours.toString().length == 1) {
          hours = '0' + hours;
        }
        if (eventKey === "call") {
          state.connectTime = { hours, min, sec };
          connectTimeInterval = this.timeInterval;
        } else {
          state.workingTime = { hours, min, sec };
          workingTimeInterval = this.timeInterval;
        }
      }, 1000);

    },
    [STOP_TIMER](state, eventKey) {
      if (eventKey === "call") {
        state.connectTime = { hours: "00", min: "00", sec: "00" }
        clearInterval(connectTimeInterval);
      } else {
        state.workingTime = { hours: "00", min: "00", sec: "00" }
        clearInterval(workingTimeInterval);
      }
    }
  }
}