import axios from "axios";
import router from "../router";

import { Toast, ADD_TOAST_MESSAGE } from "vuex-toast";

export const AUTH_REQ = "AUTH_REQ";
export const SET_TOKEN = "SET_TOKEN";
export const LOGOUT = "LOGOUT";

const AUTH_FAIL_MSG = "Логин или пароль введены неверно";

export default {
  state: {
    token: ""
  },
  actions: {
    [AUTH_REQ]({ commit }, payload) {
      axios
        .post("/login", payload)
        .then(resp => {
          if (resp.data.result) {
            commit(SET_TOKEN, resp.data.token);
            localStorage.setItem("token", resp.data.token);
            axios.defaults.headers.common["token"] = resp.data.token;
            router.push("/");
          } else {
            this.addToast
            commit(ADD_TOAST_MESSAGE, {
              type: "warning",
              text: AUTH_FAIL_MSG
            });
          }
        })
        .catch(err => {
          console.log(err);
          commit(ADD_TOAST_MESSAGE, {
            type: "warning",
            text: AUTH_FAIL_MSG
          });
        });
    },
    [LOGOUT]({ commit }) {
      axios
        .get("/logout")
        .then(() => {
          commit(SET_TOKEN, "");
          axios.defaults.headers.common["token"] = "";
          localStorage.removeItem("token");
          router.push("/login");
        })
        .catch(() => {
          commit(SET_TOKEN, "");
          axios.defaults.headers.common["token"] = "";
          localStorage.removeItem("token");
          router.push("/login");
        });
    }
  },
  mutations: {
    [SET_TOKEN](state, payload) {
      state.token = payload;
    }
  }
};
