export const SHOW_CALLER = "SHOW_CALLER";

export default {
  state: {
    caller: ""
  },
  mutations: {
    [SHOW_CALLER](state, payload) {
      if (payload) {
        state.caller = payload;
      } else {
        state.caller = "";
      }
    }
  }
}