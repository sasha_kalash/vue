export const SET_OPERATOR_INFO = "SET_OPERATOR_INFO ";

export default {
  state: {
    operator: []
  },
  mutations: {
    [SET_OPERATOR_INFO](state, payload) {
      state.operator = payload;
    }
  }
}