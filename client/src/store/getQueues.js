export const ADD_TO_QUEUES_LIST = "ADD_TO_QUEUES_LIST";
export const SET_TO_QUEUE_CHANGES = "SET_TO_QUEUE_CHANGES";


import { ADD_TO_QUEUE } from "@/constants/queueStatuses";
import { REMOVE_FROM_QUEUE } from "@/constants/queueStatuses";
import { REMOVE_FROM_ALL_QUEUES } from "@/constants/queueStatuses";
import { PROGRESS_BAR_SHOW } from "@/constants/queueStatuses";
import { ALL_PROGRESS_BAR_SHOW } from "@/constants/queueStatuses";


import { INCREMENT } from "@/constants/queueStatuses";
import { DECREMENT } from "@/constants/queueStatuses";

export default {
  state: {
    queues: []
  },
  mutations: {
    [ADD_TO_QUEUES_LIST](state, payload) {
      state.queues = payload;
    },
    [SET_TO_QUEUE_CHANGES](state, payload) {
      if (payload.key == REMOVE_FROM_ALL_QUEUES) {
        state.queues.forEach(queue => {
          if (queue.isActive) {
            queue.isConnecting = true;
          } 
        });
        return;
      }
      state.queues.find(item => {
        if (item.queue == payload.queueNumber) {
          switch (payload.key) {
            case PROGRESS_BAR_SHOW:
            item.isConnecting = !item.isConnecting;
              break;
            case ADD_TO_QUEUE:
            item.isActive = true;
              break;
            case REMOVE_FROM_QUEUE:
            item.isActive = false;
              break;
            case INCREMENT:
            item.calls++;
              break;
            case DECREMENT:
            item.calls > 0 ? item.calls-- : null;
              break;
          }
        }
      });
    }
  }
}