import { ADD_TO_QUEUE } from "@/constants/queueStatuses";
import { REMOVE_FROM_QUEUE } from "@/constants/queueStatuses";
import { REMOVE_FROM_ALL_QUEUES } from "@/constants/queueStatuses";
import { PROGRESS_BAR_SHOW } from "@/constants/queueStatuses";

import { SET_TO_QUEUE_CHANGES } from "@/store/getQueues";

export const ENTER_EXIT_QUEUE = "ENTER_EXIT_QUEUE";
export const CHANGE_QUEUE_STATUS = "CHANGE_QUEUE_STATUS";

export default {
  state: {
    queueToChange: [],
    queueAction: ""
  },
  actions: {
    [ENTER_EXIT_QUEUE]({ state, commit, rootState }, queue) {
      if (!queue) {
        state.queueAction = REMOVE_FROM_ALL_QUEUES;
        commit(SET_TO_QUEUE_CHANGES, { key: REMOVE_FROM_ALL_QUEUES });
      } else {
        commit(SET_TO_QUEUE_CHANGES, { queueNumber: queue.queue, key: PROGRESS_BAR_SHOW });
        state.queueToChange = queue.queue;
        state.queueAction = queue.isActive ? REMOVE_FROM_QUEUE : ADD_TO_QUEUE;
      }
      rootState.event.websocket.send(JSON.stringify({ "status": state.queueAction, "queue": state.queueToChange }));
      state.queueToChange = [];
    },
    [CHANGE_QUEUE_STATUS]({ state, commit }, queue) {
      if (state.queueAction == REMOVE_FROM_ALL_QUEUES) {
        commit(SET_TO_QUEUE_CHANGES, { queueNumber: queue, key: REMOVE_FROM_QUEUE });
      } else {
        commit(SET_TO_QUEUE_CHANGES, { queueNumber: queue, key: state.queueAction });
      }
      commit(SET_TO_QUEUE_CHANGES, { queueNumber: queue, key: PROGRESS_BAR_SHOW });
    }
  }
}