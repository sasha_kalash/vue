import {
  TAKE_BREAK
} from "@/store/ui";
import {
  ENTER_EXIT_QUEUE
}
from "@/store/manageQueues";

export const UPDATE_TIMER = "UPDATE_TIMER";
export const START_BREAK_TIMER = "START_BREAK_TIMER";
export const STOP_BREAK_TIMER = "STOP_BREAK_TIMER";

export const timeToRest = 10; //здесь выставляется время перерыва оператора


export default {
  state: {
    restingTime: timeToRest
  },
  methods: {

  },
  mutations: {
    [STOP_BREAK_TIMER]() {
      clearInterval(this.interval);
    },
    [UPDATE_TIMER](state) {
      setTimeout(() => {
        state.restingTime = timeToRest;
      }, 500)
    },
  },
  actions: {
    [START_BREAK_TIMER]({ commit, dispatch, state }) {
      dispatch(ENTER_EXIT_QUEUE);
      this.interval = setInterval(() => {
        if (state.restingTime > 1) {
          state.restingTime--;
        } else {
          commit(TAKE_BREAK);
          commit(STOP_BREAK_TIMER);
          commit(UPDATE_TIMER);
        }
      }, 1000)
    },
  }
}