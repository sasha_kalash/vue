import queue from '@/constants/modal';

const modal = {
    isOpenModal: false,
    header: '',
    body: '',
    footer: ''
};

const ui = {
    state: {
        websocket: {

        },
        operatorNumber: '',
        workStatus: false,
        acceptedCalls: '',
        workTime: 0,
        modal,
        queues: [{
            number: '777',
            name: 'Azino',
            isActive: true
        }]
    },
    mutations: {
        getInitialState(state, payload) {
            state.workStatus = true;
            state.queues = [state.queues, payload.queues];
            state.operatorNumber = payload.operatorNumber;
        },
        showModal(state, payload) {
            if (!state.modal.isOpenModal) {
                state.modal = { ...state.modal,
                    isOpenModal: true,
                    ...payload
                }
            } else {
                state.modal = {...state.modal, isOpenModal: false}
            }
        },
        clearModal(state) {
            state.modal = {...modal}
        }
    },
    actions: {
        showQueueModal({
            commit
        }) {
            commit('showModal', queue);
        },
        closeModal({
            commit
        }) {
            commit('showModal');
            setTimeout(() => commit('clearModal'), 500);
        }
    },

};

export default ui;