import { LOGOUT } from "@/store/user";

export const OPEN_WS = "OPEN_WS";
export const CLOSE_WS = "CLOSE_WS";
export const SET_WS = "SET_WS";
export const MESSAGE_WS = "MESSAGE_WS";

const AUTH_RESP = "AUTH_RESP";

export default {
  state: {
    websocket: {},
    events: {}
  },
  actions: {
    [OPEN_WS]({ commit, dispatch, rootState }) {
      const ws = new WebSocket(`ws://${location.hostname}:5000/operatorws`);

      ws.onopen = () => {
        ws.send(
          JSON.stringify({ token: rootState.user.token, status: "AUTH_REQ" })
        );
      };
      ws.onmessage = msg => {
        const data = JSON.parse(msg.data);
        if (data.status == AUTH_RESP) {
          if (!data.result) dispatch(LOGOUT);
          else console.log("Auth success");
        } else {
          commit(MESSAGE_WS, data);
        }
      };
      commit(SET_WS, ws);
    },
    [CLOSE_WS]({ commit, state }) {
      state.websocket.close();
      commit(SET_WS, {});
    }
  },
  mutations: {
    [SET_WS](state, payload) {
      state.websocket = payload;
    },
    [MESSAGE_WS](state, payload) {
      switch (payload.status) {
        case "newChannel":
          state.events = payload;
          break;

        case "bridge":
          state.events = payload;
          break;

        case "hangUp":
          state.events = {};
          break;

        // case "deleteHangup":
        //     Vue.delete(state.socket.messages, payload.dnid);
        //     break;

        // case "transfer":
        //     Vue.set(state.socket.messages, payload.dnid, payload);
        //     break;

        default:
          console.log("Unknown msg status", payload);
          break;
      }
    }
  }
};
