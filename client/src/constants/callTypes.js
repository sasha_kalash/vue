export const COMPLETE_CALL = "completeCall";
export const MISSED_CALL = "missedCall";
export const DIAL_EXTEN = "dialExten";
export const CONNECT_EXTEN = "connectExten";
export const QUEUE_IN = "queue_in";
export const HANG = "hang";
export const NO_TAKE_HANG = "NoTakeHang";
