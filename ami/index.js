const AmiClient = require("asterisk-ami-client"),
  log = require("../services/logger")("ami"),
  { ami } = require("../config"),
  client = new AmiClient(ami.options);

module.exports.getClient = () => client;

const { onEventHandler, onUserEventHandler } = require("./onEvent");

module.exports.amiStart = () => {
  client
    .connect(
      ami.login,
      ami.password,
      {
        host: ami.host,
        port: ami.port
      }
    )
    .then(amiConnection => {
      client
        .on("connect", () => log.info("Successfully connected to asterisk"))
        .on("event", async event => {
          // console.log(event);
          onEventHandler(event);
        })
        .on("UserEvent", async event => {
          // console.log(event);
          onUserEventHandler(event);
        })
        .action({
          Action: "Sippeers"
        });
    })
    .catch(error => log.error(error));
};
