const queuelist = (module.exports.queuelist = {}),
  iplist = (module.exports.iplist = {}),
  { operatorWs } = require("../src/middleware/wsroutes"),
  log = require("../services/logger")("ami"),
  state = {},
  { queues, decrement } = require("../services/postgres");


module.exports.onEventHandler = event => {
  // console.log(event)
  const ident = event.ActionID;
  switch (event.Event) {
    case "QueueMember":
      if (event.Name === `Local/${ident}@dialout`) {
        queues[ident].forEach(queue => {
          if (queue.queue === event.Queue) {
            queue.isActive = true;
          }
        });
      }
      break;
    case "QueueParams":
      queues[ident].forEach(queue => {
        if (queue.queue === event.Queue) {
          queue.calls = event.Calls;
        }
      });
      break;
    case "QueueStatusComplete":
      operatorWs[ident].ws.send(
        JSON.stringify({
          oper: ident,
          name: "queues",
          data: queues[ident],
          status: "INITIAL_STATE_RES"
        })
      );
      break;
      case "PeerEntry":
      iplist[event.ObjectName] = event.IPaddress;
      break;
      case "PeerlistComplete":
      console.log(iplist);
      break;
  }
};

module.exports.onUserEventHandler = event => {
  console.log(event.UserEvent);
  const { UserEvent } = event;
  const data = {
    id: event.id,
    status: UserEvent,
    callerNumber: event.cid, // callerNumber
    exten: event.did, // destination number/queue
    date: Date.now(),
    state: "",
    waiting: ""
  };
  const destination = event.dest.split(" ");
  // log.info(destination);
  switch (UserEvent) {
    case "outgoingCall":
      destination.forEach(destin => {
        dest = Number(destin);
        try {
          operatorWs[dest].ws.send(JSON.stringify(data));
          log.info(`Звонок оператора ${dest} в город`);
        } catch (e) {
          log.info(`Оператор ${dest} не авторизирован`);
        }
      });
      break;
    case "queue_in":
    // console.log(event);
      log.info("Звонок пришёл в очередь");
      // console.log(event.queue);
      state[event.id] = { time: +new Date() };
      destination.forEach(destin => {
        const dest = Number(destin);
        try {
          operatorWs[dest].ws.send(JSON.stringify(data));
          log.info(`Звонок пришёл в очередь`);
        } catch (e) {
          log.info(`Опертор ${dest} не авторизирован`);
        }
      });
      break;
    case "dialExten":
      log.info(`Оператор ${event.did} взял трубку`);
      decrement(event.queue);
      destination.forEach(destin => {
        dest = Number(destin);
        try {
          operatorWs[dest].ws.send(JSON.stringify(data));
          log.info(`Оператор ${event.did} взял трубку`);
        } catch (e) {
          log.info(`Оператор ${dest} не авторизирован`);
        }
      });
      break;
    case "connectExten":
      // console.log(event)
      log.info(`Звонок дошёл оператору ${destination}`);
      destination.forEach(destin => {
        dest = Number(destin);
        // console.log(state[event.id] ? +new Date() - state[event.id].time : -1);
        try {
          operatorWs[dest].ws.send(
            JSON.stringify({
              ...data,
              waitTime: state[event.id]
                ? +new Date() - state[event.id].time
                : -1
            })
          );
          log.info(`Звонок дошёл на фронт оператору ${dest}`);
        } catch (e) {
          log.info(`Опертор ${dest} не авторизирован`);
        }
      });
      break;
    case "hang":
      // console.log(event)
      log.info("Конец разговора");
      delete state[event.id];
      destination.forEach(destin => {
        dest = Number(destin);
        try {
          if (event.State === "missedCall") {
            log.info(`Оператор ${dest} не взял трубку`);
            data.state = "missedCall";
            dest = Number(destin);
            decrement(event.queue);
            try {
              operatorWs[dest].ws.send(JSON.stringify(data));
              log.info(`Оператор ${dest} не взял трубку`);
            } catch (e) {
              log.info(`Оператор ${dest} не авторизирован`);
            }
          } else if (event.State === "localCall") {
            log.info(`Оператор ${dest} положил трубку`);
            data.state = "completeCall";
            dest = Number(destin);
            try {
              operatorWs[dest].ws.send(JSON.stringify(data));
              log.info(`Оператор ${dest} положил трубку`);
            } catch (e) {
              log.info(`Оператор ${dest} не авторизирован`);
            }
            
            } 
            else if(event.State === 'Blindxfer'){
              data.state = "blindTransfer";
              try {
                operatorWs[dest].ws.send(JSON.stringify(data));
                log.info(`Оператор ${dest} окончил трансфер`);
              } catch (e) {
                log.info(`Оператор ${dest} не авторизирован`);
              }
            }
            else if(event.State === 'Atxfer'){
              data.state = "atendentTransfer";
              try {
                operatorWs[dest].ws.send(JSON.stringify(data));
                log.info(`Оператор ${dest} окончил трансфер`);
              } catch (e) {
                log.info(`Оператор ${dest} не авторизирован`);
              }
            }
            else if (event.did !== event.dest) {
            data.state = "NoTakeHang";
            log.info(
              `Оператор ${dest} не взял трубку в связи с ответом другого оператора`
            );
            operatorWs[dest].ws.send(JSON.stringify(data));
          } else {
            data.state = "completeCall";
            log.info(`Конец разговора c оператором ${dest}`);
            operatorWs[dest].ws.send(JSON.stringify(data));
          }
        } catch (e) {
          log.info(`Оператор ${dest} не авторизирован`);
        }
      });
      break;
    case "oper_unreg":
      destination.forEach(destin => {
        dest = Number(destin);
        try {
          log.info("Оператор вышел из очереди");
          operatorWs[dest].ws.send(
            JSON.stringify({
              status: "QUEUE_ACTION_RES",
              result: true,
              info: "success",
              queue: event.Exten.split('*')[0]
            })
          );
        } catch (e) {
          log.info(`Оператор ${dest} не авторизирован`);
        }
      });
      break;
    case "oper_reg":
      destination.forEach(destin => {
        dest = Number(destin);
        try {
          log.info("Оператор вышел из очереди");
          // console.log(event)
          operatorWs[dest].ws.send(
            JSON.stringify({
              status: "QUEUE_ACTION_RES",
              result: true,
              info: "success",
              queue: event.Exten.split('*')[0]
            })
          );
        } catch (e) {
          log.info(`Оператор ${dest} не авторизирован`);
        }
      });
      break;
    case "transferBegin":
    destination.forEach(destin => {
      dest = Number(destin);
      try {
        log.info("Трансфер начался");
        operatorWs[dest].ws.send(JSON.stringify(data));
      }
      catch (e) {
        log.info(`Оператор ${dest} не авторизирован`);
      }
    });
      break;
    case "transferConnect":
    destination.forEach(destin => {
      dest = Number(destin);
      try {
        log.info(`Трансфер ${dest} дощёл оператору`);
        operatorWs[dest].ws.send(JSON.stringify(data));
      }
      catch (e) {
        log.info(`Оператор ${dest} не авторизирован`);
      }
    });
      break;
    case "transferSuccess":
    destination.forEach(destin => {
      dest = Number(destin);
      try {
        log.info(`Оператор ${dest} ответил на трансфер`);
        operatorWs[dest].ws.send(JSON.stringify(data));
      }
      catch (e) {
        log.info(`Оператор ${dest} не авторизирован`);
      }
    });
      break;
    case "NewConference":
      log.info("Конференция создана");
      operatorWs.forEach(oper => oper.ws.send(JSON.stringify(data)));
      break;
    case "InviteUser":
      data.callerNumber = event.did;
      operatorWs.forEach(oper => oper.ws.send(JSON.stringify(data)));
      log.info("В конференцию добавлен участник");
      break;
    case "EndConference":
      operatorWs.forEach(oper => oper.ws.send(JSON.stringify(data)));
      log.info("Конференция завершена");
      break;
    case "LeaveUser":
      operatorWs.forEach(oper => oper.ws.send(JSON.stringify(data)));
      log.info("Конференция завершена");
      break;
  }
};
