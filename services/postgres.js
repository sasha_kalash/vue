const { Pool } = require("pg");
const queues = {};
const queuelist = [];
const { operatorWs } = require("../src/middleware/wsroutes");
// const calls = [];
const { changequeue } = require("../services/asteriskcommand");
const request = require("request");


const pool = new Pool({
  user: "postgres",
  host: "192.168.5.119",
  database: "cdr",
  password: "",
  port: "5432"
});

const pool1 = new Pool({
  user: "postgres",
  host: "192.168.5.119",
  database: "pbx",
  password: "",
  port: "5432"
});

const callhistory = async function callhistory(data1, data2, ident) {
  const callh = await pool.query(
  `select * from cc_cdr where time > '${data1}' and time < '${data2}' and (type = 'local call' or type = 'incoming call')`
  );
  const callhistorylist = [];
  callh.rows.forEach(rows => {
    data = {
      id: Number(rows.linked_id),
      // status: 'answered',
      callerNumber: rows.caller_id,
      exten: rows.called_number,
      operator: rows.answered,
      date: rows.time ,
      duration: Number(rows.billsec),
      waiting: Number(rows.duration),
      state: 'completeCall'
    };
    if (rows.answered === 'n/a'){
      data.state = 'missedCall'
    }
    callhistorylist.push(data);
  })
  console.log(callhistorylist)
  operatorWs[ident].ws.send(
    JSON.stringify({ status: `CALL_HISTORY_SEARCH_RES`, data: callhistorylist })
    );
}

const queuestates = async function queuestates() {
  const queuestate = await pool1.query(
    `select p1, cid from outgoing_extensions;`
  );
  // console.log(queuestate.rows);
  queuestate.rows.forEach(queue => {
    if (queues[queue.cid] && queue.cid !== "self") {
      queues[queue.cid].push({ queue: queue.p1 });
    } else if (queue.cid !== "self") {
      queues[queue.cid] = [];
      queues[queue.cid].push({ queue: queue.p1 });
    }
  });
  // console.log(queues);
};

const checkqueue = async function checkqueue(ident, queue, command) {
  queues[ident].forEach(async queuec => {
    if (queuec.queue === queue){
      const date = new Date();
      let ch = "ADD";
      const formated_date =
        date.getFullYear() +
        "_" +
        ("0" + (date.getMonth() + 1)).slice(-2) +
        "_" +
        ("0" + date.getDate()).slice(-2);
      const check = await pool.query(
        `select log_out from cc_agent where queue = '${queue}' and agent = '${ident}';`
      );
      check.rows.forEach(rows => {
        if (rows.log_out === null) {
          ch = "REMOVE";
        }
        else if (check.rowCount === 0){
          ch = "ADD";
        } 
      });
      if (ch === command) {
        changequeue(ident, queue);
      } else {
        operatorWs[ident].ws.send(
          JSON.stringify({ status: `QUEUE_ACTION_RES`, result: false, queue })
          );
        }
    }
  })
};

const decrement = async function decrement(queue){
  Object.keys(queues).forEach((key) => { 
    queues[key].forEach(ch => {
      if (ch.queue === queue) {
        try{
        operatorWs[key].ws.send(JSON.stringify({status: `DECREMENT` ,exten: queue}))
        }
        catch(e){
          // console.log(`Оператор ${key} не авторизирован`);
        }
      }
    })
  })
}

const removefromallqueue = async function removefromallqueue(ident){
  const date = new Date();
  const formated_date =
    date.getFullYear() +
    "_" +
    ("0" + (date.getMonth() + 1)).slice(-2) +
    "_" +
    ("0" + date.getDate()).slice(-2);
  const check = await pool.query(
  `select queue , log_out from cc_agent where agent = '${ident}';`
  );
  // console.log(check.rows);
  check.rows.forEach(rows => {
    if (rows.log_out === null) {
      changequeue(ident, rows.queue);
    }
  })
}

const select = async function select(ident) {
  const calls = []
  const date = new Date();
  const formated_date =
    date.getFullYear() +
    "_" +
    ("0" + (date.getMonth() + 1)).slice(-2) +
    "_" +
    ("0" + date.getDate()).slice(-2);
  const selectdata = await pool.query(
    `select * from cc_cdr where ((type = 'local call' and (exten = '${ident}' or caller_id = '${ident}')) or (type = 'incoming call' and answered = '${ident}')
    or ((type = 'local call' and exten = '${ident}' and answered = 'n/a') or (type = 'incoming call' and answered = 'n/a'))) and (time > '${formated_date}');`
  );

  selectdata.rows.forEach(rows => {
  rows.time.setHours(rows.time.getHours()+3)
  data = {
    id: Number(rows.linked_id),
    // status: 'answered',
    callerNumber: rows.caller_id,
    exten: rows.called_number,
    // operator: rows.answered,
    date: rows.time ,
    duration: Number(rows.billsec),
    waiting: Number(rows.duration),
    state: 'completeCall'
  };
  if ((rows.type === 'local call' && rows.answered === 'n/a') || (rows.type === 'incoming call' && rows.answered === 'n/a')){
    data.state = 'missedCall'
  }
  calls.push(data);
});
  operatorWs[ident].ws.send(
    JSON.stringify({ data: calls, status: "CALLS_HISTORY_RES" })
  );
};

const update = async function update(
  oldData,
  newData,
  tableName,
  additionallyParams = ""
) {
  await pool.query(
    `update ${oldData} set ${newData} from ${tableName} ${additionallyParams};`
  );
};

module.exports = {
  update,
  select,
  queuestates,
  queuelist,
  queues,
  checkqueue,
  callhistory,
  removefromallqueue,
  decrement
};
