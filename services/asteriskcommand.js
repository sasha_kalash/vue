"use strict";
const { getClient } = require("../ami");

module.exports.queueGet = ident => {
  const aster = getClient();
  aster.action({ Action: "QueueStatus", ActionID: ident }, true);
};

module.exports.changequeue = (ident, queue) => {
  const aster = getClient();
  aster.action({
    Action: "Originate",
    Channel: `Local/${queue}*${ident}@iax`,
    Priority: '1' ,
    Callerid: ident,
    Context:'iax',
    Exten: `${queue}*${ident}`
  });
};

module.exports.call = (ident, number) => {
  const aster = getClient();
  aster.action({
    Action: "Originate",
    Channel: `SIP/CC+x${ident}`,
    Priority: '1',
    Callerid: `${ident}`,
    Context:'iax',
    Exten: `${number}`,
    Variable: 'SIPADDHEADER="Call-Info:\;answer-after=0"'
  });
};

module.exports.Sippeers = () => {
  const aster = getClient();
  aster.action({
    Action: `Sippeers`
  });
};

module.exports.test = () => {
  const aster = getClient();
  aster.action({ Action: `CoreShowChannels` });
};
