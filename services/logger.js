const winston = require("winston");

const logLevel = process.env.NODE_ENV == "production" ? "info" : "debug";

module.exports = loggerName =>
	winston.createLogger({
		level: logLevel,
		format: winston.format.combine(
			winston.format.label({ label: loggerName }),
			winston.format.colorize(),
			winston.format.align(),
			winston.format.timestamp({ format: "YYYY-MM-DD HH:mm:ss" }),
			winston.format.printf(info => {
				return `${info.timestamp} [${info.label}] ${info.level}: ${
					info.message
				}`;
			})
		),
		transports: [new winston.transports.Console()]
	});
