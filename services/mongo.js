"use strict";

const mongo = require("mongodb"),
	{
		MONGO
	} = require("../config");


let db = null,
	client = null;

module.exports.initializeDb = callback => {
	mongo.MongoClient.connect(
		MONGO.url, {
			useNewUrlParser: true
		},
		(err, cl) => {
			if (err) return callback(err);
			client = cl;
			db = client.db(MONGO.name);
			return callback(err);
		}
	);
};

module.exports.getDb = () => db;
module.exports.getClient = () => client;

module.exports.find = async (collection, params) =>
	await db
		.collection(collection)
		.find(params)
		.toArray();
module.exports.queue = (ident) => {
	console.log('----------------');
	const aster = getClient();
	aster.action({ Action: 'QueueStatus', ActionID: ident }, true);
};
module.exports.findOne = async (collection, params) =>
	await db.collection(collection).findOne(params);
module.exports.findOneById = async (collection, id) =>
	await db.collection(collection).findOne({
		_id: new mongo.ObjectID(id)
	});
module.exports.insertOne = async (collection, params) =>
	await db.collection(collection).insertOne(params);
module.exports.deleteOneByID = async (collection, params) =>
	await db
		.collection(collection)
		.deleteOne({
			_id: new mongo.ObjectID(params)
		});
module.exports.deleteOne = async (collection, params) =>
	await db.collection(collection).deleteOne(params);
module.exports.update = async (collection, obj, data) =>
	await db
		.collection(collection)
		.updateOne(obj, {
			$set: data
		}, {
				upsert: true
			});

module.exports.updateMany = async (collection, obj, data) =>
	await db
		.collection(collection)
		.updateMany(obj, {
			$set: data
		}, {
				upsert: true
			});

module.exports.updateOneWithId = async (collection, params) => {
	const data = {
		...params
	};
	delete data._id;
	return await db
		.collection(collection)
		.updateOne({
			_id: new mongo.ObjectID(params._id)
		}, {
				$set: data
			});
};
exports.updateOne = async (collection, params) => {
	const data = {
		...params
	};
	delete data._id;
	return await db.collection(collection).updateOne({
		_id: new mongo.ObjectID(params._id)
	}, {
			$set: data
		});
};