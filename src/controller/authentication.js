const crypto = require("crypto");

const { findOne, find, insertOne, deleteOne } = require("../../services/mongo");

const generateToken = async (user) => {
    let token = crypto.createHash('md5').update(user + Math.random().toString(36).substring(5)).digest("hex");
    let tokens = await find('sessions');
    tokens = tokens.map(elem => elem.token);
    while (tokens.includes(token)) {
        token = crypto.createHash('md5').update(user + Math.random().toString(36).substring(5)).digest("hex");
    }
    
    return token;
};

exports.login = async (ctx, next) => {
    // body = {login, password}
    const user = await findOne("users", ctx.request.body);
    if (user) {
        let token = await generateToken(user.login);
        await insertOne('sessions', {user_id: user._id, token, callId: user.callId});
        ctx.status = 200;
        ctx.body = {"result": true, "msg": "authentication succeeded", token};

    } else {
        ctx.status = 401;
        ctx.body = {
            "result": false,
            "msg": "Unknown login or password"
        };
    }
};

exports.logout = async (ctx, next) => {
    const token = ctx.request.header.token || ctx.request.body.token || null;
    if (token) {
        await deleteOne("sessions", {token})
    }
    ctx.body = {"result": true};
};