const findOne = require("../../services/mongo");

module.exports = async (ctx, next) => {
  const token = ctx.request.header.token || ctx.request.body.token || null;
  const session = await findOne("sessions", { token });
  if (session) {
    ctx.state.userId = session.user_id;
    ctx.state.token = token;
    ctx.state.callId = session.callId;
    await next();
  } else {
    ctx.status = 401;
    ctx.body = { result: false, msg: "Authentication failed" };
  }
};
