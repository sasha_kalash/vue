const websockify = require("koa-websocket"),
  Router = require("koa-router"),
  logger = require("../../services/logger")("ws"),
  { findOne, findOneById, find } = require("../../services/mongo"),
  wsr = new Router(),
  operatorWs = (module.exports.operatorWs = []),
  AUTH_REQ = "AUTH_REQ",
  AUTH_RESP = "AUTH_RESP",
  INITIAL_STATE_REQ = "INITIAL_STATE_REQ",
  ADD_TO_QUEUE = "ADD_TO_QUEUE",
  ident = "",
  { queues, select, checkqueue, removefromallqueue, callhistory } = require("../../services/postgres"),
  { iplist } = require("../../ami/onEvent"),
  { queueGet, changequeue, call, pickup } = require("../../services/asteriskcommand");
  const request = require("request");
  


module.exports.websockified = app => {
  const wsapp = websockify(app);

  wsapp.ws.use(
    wsr
      .all("/operatorws", async ctx => {
        logger.debug("Client connected, waiting for auth");
        let authenticated = false,
          user = {},
          token = null;

        ctx.websocket.on("message", async message => {
          try {
            const msg = JSON.parse(message);
            if (!authenticated && msg.status === AUTH_REQ) {
              token = msg.token;
              const session = await findOne("sessions", {
                token
              });
              if (!session) {
                ctx.websocket.send(
                  JSON.stringify({
                    status: AUTH_RESP,
                    result: false,
                    data: "Invalid token"
                  })
                );
                ctx.websocket.terminate();
              } else {
                authenticated = true;
                user = {
                  token,
                  ...(await findOneById("users", session.user_id))
                };
                operatorWs[session.callId] = {
                  ws: ctx.websocket,
                  userId: user._id,
                  token,
                  user
                };
                ctx.websocket.send(
                  JSON.stringify({
                    status: AUTH_RESP,
                    // user,
                    result: true
                  })
                );
                logger.debug(`Client successfully authenticated`);
              }
            } else if (!authenticated) {
              ctx.websocket.terminate();
            } else if (msg.status === INITIAL_STATE_REQ) {
              operatorWs.forEach(key => {
                if (ctx.websocket === key.ws) {
                  const ident = Number(key.user.callId);
                  queues[ident].forEach(queue =>{
                    queue.isActive = false
                    queue.isConnecting = false
                  })
                  queueGet(ident);
                }
              });
            } 
            else if (msg.status === 'CALLS_HISTORY_REQ') {
              operatorWs.forEach(key => {
                if (ctx.websocket === key.ws) {
                  const ident = Number(key.user.callId);
                  queues[ident].forEach(queue =>{
                    queue.isActive = false
                  })
                  select(ident);
                }
              });
            } 
            else if (msg.status === ADD_TO_QUEUE){
              operatorWs.forEach(key => {
                if (ctx.websocket === key.ws) {
                  const ident = Number(key.user.callId);
                  checkqueue(ident, msg.queue, 'ADD')
                }
              })
            }
            else if (msg.status === 'REMOVE_FROM_QUEUE'){
              operatorWs.forEach(key => {
                if (ctx.websocket === key.ws) {
                  const ident = Number(key.user.callId);
                  // setTimeout(() => { //для демонстрации прогресс-бара
                    checkqueue(ident, msg.queue, 'REMOVE')
                  // }, 3000)
                }
              })
            }
            else if (msg.status === 'REMOVE_FROM_ALL_QUEUES'){
              operatorWs.forEach(key => {
                if (ctx.websocket === key.ws) {
                  const ident = Number(key.user.callId);
                  removefromallqueue(ident)
                }
              })    
            }
            else if (msg.status === 'PHONEBOOK_REQ'){
              operatorWs.forEach(async key => {
                if (ctx.websocket === key.ws) {
                    const ident = Number(key.user.callId);
                    const book1 = await find("users")
                    const book = []
                    book1.forEach (element => {
                      delete element.password
                      delete element._id
                      if (element.callId !== ident){
                        book.push({name: element.login, number: element.callId})
                      }
                    });
                    ctx.websocket.send(JSON.stringify({status: `PHONEBOOK_RES` ,data: book}))
                  }
              })
            
          }
            else if (msg.status === 'CALL'){
              operatorWs.forEach(key => {
                if (ctx.websocket === key.ws) {
                  const ident = Number(key.user.callId);
                  call(ident, msg.number)
                }
              })  
            }
            else if (msg.status === 'PICKUP') {
              operatorWs.forEach(key => {
                if (ctx.websocket === key.ws) {
                  const ident = Number(key.user.callId);
                  // console.log(ident)
                  request
                    .get(`http://${iplist[`CC+x${ident}`]}/cgi-bin/cgiServer.exx?key=OK`)
                    .auth("admin", "admin", false);
                }
              })
            }
            else if (msg.status === 'HANG') {
              operatorWs.forEach(key => {
                if (ctx.websocket === key.ws) {
                  const ident = Number(key.user.callId);
                  // console.log(ident)
                  request
                  .get(`http://${iplist[`CC+x${ident}`]}/cgi-bin/cgiServer.exx?key=X`)
                    .auth("admin", "admin", false);
                }
              })
            }
            else if (msg.status === 'CALL_HISTORY_SEARCH_REQ'){
            operatorWs.forEach(key => {
              if (ctx.websocket === key.ws) {
              const ident = Number(key.user.callId);
              const dateFrom = new Date(msg.data.dateFrom)
              const data1 = dateFrom.getFullYear()+'-'+("0" + (dateFrom.getMonth() + 1)).slice(-2)+'-'+("0" + dateFrom.getDate()).slice(-2)+' '+("0" + dateFrom.getHours()).slice(-2)+':'+("0" + dateFrom.getMinutes()).slice(-2)+':'+'00'
              const dateTo = new Date(msg.data.dateTo)
              const data2 = dateTo.getFullYear()+'-'+("0" + (dateTo.getMonth() + 1)).slice(-2)+'-'+("0" + dateTo.getDate()).slice(-2)+' '+("0" + dateTo.getHours()).slice(-2)+':'+("0" + dateTo.getMinutes()).slice(-2)+':'+'00'
              callhistory(data1, data2, ident)
              }
            })
            }
            else {
              logger.debug(`Incoming message - ${msg}`);
            }
          } catch (err) {
            console.log(err);
            logger.error("Message parse error");
          }
        });
      })
      .routes()
  );
  return wsapp;
};
