const Router = require("koa-router");
const { login, logout } = require("../controller/authentication");
const { find } = require("../../services/mongo");
const {
  queue,
  changequeue,
  test,
  call,
  // pickup,
  Sippeers
} = require("../../services/asteriskcommand");
const { operatorWs } = require("../middleware/wsroutes");
const {
  queuestates,
  queuelist,
  queues,
  checkqueue,
  callhistory,
  removefromallqueue,
  decrement
} = require("../../services/postgres");
const request = require("request");
const http = require('http');
const fs = require('fs');
var path = require('path');

const auth = require("./auth");
const router = new Router();
module.exports.router = router;

router.post("/login", login);
router.get("/logout", logout);
router.get("/ast", () => queue(2213));
router.get("/add", Sippeers);
router.get("/rem", decrement);
router.get("/test", async ctx => {
  const filepath = path.join(__dirname, '/audio/Test.mp3');
  const stream = fs.createReadStream(filepath);
  ctx.body = stream;
});

router.get("/data", async () => {
  const users = await find("users");
});

exports.routes = () => router.routes();
exports.allowedMethods = () => router.allowedMethods();
